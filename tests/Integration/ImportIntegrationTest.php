<?php

namespace Tests\Integration;

use Tests\Setup;
use Illuminate\Http\UploadedFile;

class ImportIntegrationTest extends Setup
{
    /**
     * Test to import spreadsheet
     *
     * @return void
     */
    public function testImportSpreadsheet()
    {
        $originalName         = "products_teste_integration.xlsx";
        $completeOriginalName = base_path("tests/Fixtures/$originalName");
        $newName              = str_random(8) . '.xlsx';

        // new UploadFile to import
        $data['spreadsheet'] = new UploadedFile(
            $completeOriginalName,
            $newName,
            mime_content_type($completeOriginalName),
            null,
            null,
            true
        );

        $response = $this->post('/api/products/import', $data);
        $response->assertStatus(200);
        $response->assertJsonStructure(['id', 'file']);
    }
}
