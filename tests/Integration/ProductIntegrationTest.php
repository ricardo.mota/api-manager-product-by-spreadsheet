<?php

namespace Tests\Integration;

use Tests\Setup;
use App\Models\Product;
use App\Models\LogJob;

class ProductIntegrationTest extends Setup
{
    /**
     * structure product const
     */
    protected const STRUCTUREPRODUCT = [
        'lm',
        'name',
        'free_shipping',
        'description',
        'price'
    ];

    /**
     * @var variable to receive product
     */
    public $product;

    public function setUp()
    {
        parent::setUp();
        $this->initDefaultData(Product::class);
        $this->product = factory(Product::class)->create();
    }

    /**
     * Test to get all products
     *
     * @return void
     */
    public function testGetAllProducts()
    {
        $response = $this->get('/api/products');

        $response->assertStatus(200);
        $response->assertJsonStructure([self::STRUCTUREPRODUCT]);
    }

    /**
     * Test to get only one product
     *
     * @return void
     */
    public function testGetProduct()
    {
        $response = $this->get('/api/products/'.$this->product->lm);

        $response->assertStatus(200);
        $response->assertJson($this->product->toArray());
    }

    /**
     * Test to get nonexistent model
     *
     * @return void
     */
    public function testGetNonexistentProduct()
    {
        $response = $this->get('/api/products/0');
        $response->assertStatus(404);
    }

    /**
     * Test to update product
     *
     * @return void
     */
    public function testUpdateProduct()
    {
        $data = [
            'name'=> "name test update",
            'description' => "test description update",
        ];

        $response = $this->put('/api/products/'.$this->product->lm, $data);
        $response->assertStatus(200);
        $response->assertJsonStructure(self::STRUCTUREPRODUCT);
        $response->assertJsonFragment($data);
    }

    /**
     * Test to delete product
     *
     * @return void
     */
    public function testDeleteProduct()
    {
        $response = $this->delete('/api/products/'.$this->product->lm);

        $response->assertStatus(200);
        $response->assertJsonStructure([self::STRUCTUREPRODUCT]);
        $response->assertJsonMissing([$this->product->name]);
    }

    /**
     * Test to get status import
     *
     * @return void
     */
    public function testGetStatusImport()
    {
        $logJob = factory(LogJob::class)->create();
        $response = $this->get('/api/products/import/status/'.$logJob->job_id);

        $response->assertStatus(200);
    }

    /**
     * Test update product invalid Lm(id)
     *
     * @return void
     */
    public function testUpdateProductInvalidLm()
    {
        $newProduct = factory(Product::class)->create();
        $data = [
            'lm' => $this->product->lm,
            'name'=> "new name",
        ];

        $response = $this->put('/api/products/'.$newProduct->lm, $data);
        $response->assertStatus(400);
    }

    /**
     * Test get nonexistent log
     *
     * @return void
     */
    public function testGetNonexistentLog()
    {
        $response = $this->get('/api/products/import/status/0');
        $response->assertStatus(404);
    }
}
