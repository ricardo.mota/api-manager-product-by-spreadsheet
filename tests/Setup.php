<?php
namespace Tests;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\DB;
use Mockery;
use App\Models\ModelInterface;
use ReflectionMethod;

class Setup extends TestCase
{
    use DatabaseTransactions;

    /**
     * @inheritdoc
     */
    public function setUp()
    {
        parent::setUp();
    }

    /**
     * @after
     */
    public function tearDown()
    {
        Mockery::close();
        DB::rollBack();
        DB::disconnect();
    }

    /**
     * Init any model data
     *
     * @param string $class
     * @param int $quantity
     * @return void
     */
    public function initDefaultData(string $class, int $quantity = 5): void
    {
        factory($class, $quantity)->create();
    }

    /**
     * Method to call private methods from class
     *
     * @param string $class
     * @param string $method
     * @param mixed $arg
     * @return mixed
     * @throws \Exception
     */
    public function callPrivateMethod(string $class, string $method, $arg = null)
    {
        $method = new ReflectionMethod($class, $method);
        $method->setAccessible(true);
        return $method->invoke(new $class, $arg);
    }
}
