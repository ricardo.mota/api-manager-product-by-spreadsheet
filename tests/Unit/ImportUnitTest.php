<?php

namespace Tests\Unit;

use App\Imports\ProductsImport;
use App\Models\Product;
use Tests\Setup;
use App\Business\ImportBusiness;
use App\Models\Category;


class ImportUnitTest extends Setup
{
    /**
     * @var $collectCategoryDefault
     */
    public $collectCategoryDefault;

    /**
     * @var $collectProductDefault
     */
    public $collectProductDefault;

    /**
     * @var $productsImport
     */
    public $productsImport;

    public function setUp()
    {
        parent::setUp();

        $this->productsImport = new ProductsImport();
        $this->collectCategoryDefault = collect([
            'Category',
            get_random_id_from_entity(new Category, 'id'),
            null,
            null,
            null
        ]);
        $this->collectProductDefault = factory(Product::class)->make();
    }

    /**
     * Test import spreadsheet
     *
     * @return void
     * @throws \Exception
     */
    public function testImportSpreadsheet()
    {
        $business     = new ImportBusiness();
        $originalName = "products_teste_integration.xlsx";
        $array        = $business->importSpreadsheet($originalName, 'fixtures');
        $this->assertArrayHasKey('id', $array);
        $this->assertArrayHasKey('file', $array);
    }

    /**
     * Test to method processCategory
     *
     * @return void
     * @throws \Exception
     */
    public function testProductsImportMethodProcessCategory()
    {
        $return = $this->callPrivateMethod(ProductsImport::class, 'processCategory', $this->collectCategoryDefault);
        $this->assertInstanceOf(Category::class, $return);
        $this->assertContains($this->collectCategoryDefault[1], $return->toArray());
    }

    /**
     * Test to method processProduct
     *
     * @return void
     * @throws \Exception
     */
    public function testProductsImportMethodProcessProduct()
    {
        $return = $this->callPrivateMethod(ProductsImport::class, 'processProduct', $this->collectProductDefault->toArray());
        $this->assertInstanceOf(Product::class, $return);
        $this->assertContains($this->collectProductDefault->lm, $return->toArray());
    }

    /**
     * Test to method validateCategoryRow
     *
     * @return void
     * @throws \Exception
     */
    public function testProductsImportMethodValidateCategoryRow()
    {
        $return = $this->callPrivateMethod(ProductsImport::class, 'validateCategoryRow', $this->collectCategoryDefault);
        $this->assertTrue($return);
    }

    /**
     * Test to method validateProductRow
     *
     * @return void
     * @throws \Exception
     */
    public function testProductsImportMethodValidateProductRow()
    {
        $productCollection = collect(array_values($this->collectProductDefault->toArray()));
        $return = $this->callPrivateMethod(ProductsImport::class, 'validateProductRow', $productCollection);
        $this->assertTrue($return);
    }

    /**
     * Test to method makeCategoryRow
     *
     * @return void
     * @throws \Exception
     */
    public function testProductsImportMethodMakeCategoryRow()
    {
        $return = $this->callPrivateMethod(ProductsImport::class, 'makeCategoryRow', $this->collectCategoryDefault);
        $this->assertEquals(['id' => $this->collectCategoryDefault[1], 'name' => 'Category'], $return);
    }

    /**
     * Test to method makeProductRow
     *
     * @return void
     * @throws \Exception
     */
    public function testProductsImportMethodMakeProductRow()
    {
        $product = $this->collectProductDefault->toArray();
        $productCollection = collect(array_values($product));

        $return = $this->callPrivateMethod(ProductsImport::class, 'makeProductRow', $productCollection);
        unset($product['category_id']);

        $this->assertEquals($product, $return);
    }

    /**
     * Test FileNotFoundException from get a product
     *
     * @expectedException \App\Exceptions\FileNotFoundException
     * @expectedExceptionMessage was not found
     */
    public function testFileNotFoundException()
    {
        $business     = new ImportBusiness();
        $originalName = "no.xlsx";
        $business->importSpreadsheet($originalName, 'fixtures');
    }
}
