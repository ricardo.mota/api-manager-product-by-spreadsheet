<?php

namespace Tests\Unit;

use Mockery;
use Tests\Setup;
use Illuminate\Support\Collection;
use App\Business\CategoryBusiness;
use App\Builders\ResourceBuilder;
use App\Models\Category;

class CategoryUnitTest extends Setup
{
    /**
     * @var mockCategoryModel
     */
    private $mockCategoryModel;

    /**
     * @var mockResourceBuild
     */
    private $mockResourceBuild;

    /**
     * @var defaultCategory
     */
    private $defaultCategory;


    public function setUp()
    {
        parent::setUp();
        $this->mockCategoryModel = Mockery::mock(Category::class);
        $this->mockResourceBuild = Mockery::mock(ResourceBuilder::class);
        $this->defaultCategory = factory(Category::class)->create();
    }

    /**
     * Test to get a category collection
     *
     * @return void
     */
    public function testGetAllCategories()
    {
        $this->mockCategoryModel->allows([
            "get" => collect([$this->defaultCategory]),
        ]);

        $business = new CategoryBusiness($this->mockCategoryModel, $this->mockResourceBuild);
        $this->assertEquals(collect([$this->defaultCategory]), $business->getCategories());
        $this->assertInstanceOf(Collection::class, $business->getCategories());
    }

    /**
     * Test to get a category
     *
     * @return void
     */
    public function testGetCategory()
    {
        $this->mockCategoryModel->allows([
            "find" => $this->defaultCategory,
        ]);

        $business = new CategoryBusiness($this->mockCategoryModel, $this->mockResourceBuild);
        $this->assertEquals($this->defaultCategory, $business->getCategory(1));
        $this->assertInstanceOf(Category::class, $business->getCategory(1));
    }

    /**
     * Test to create category
     *
     * @return void
     */
    public function testCreateCategory()
    {
        $this->mockResourceBuild->allows([
            "buildCreate" => $this->defaultCategory,
        ]);

        $business = new CategoryBusiness($this->mockCategoryModel, $this->mockResourceBuild);
        $this->assertEquals($this->defaultCategory, $business->storeCategory([]));
        $this->assertInstanceOf(Category::class, $business->storeCategory([]));
    }

    /**
     * Test to update a category
     *
     * @return void
     */
    public function testUpdateCategory()
    {
        $this->mockCategoryModel->allows([
            "find" => $this->defaultCategory,
        ]);
        $this->mockResourceBuild->allows([
            "buildUpdate" => $this->defaultCategory,
        ]);

        $business = new CategoryBusiness($this->mockCategoryModel, $this->mockResourceBuild);
        $this->assertInstanceOf(Category::class, $business->updateCategory(1, []));
    }

    /**
     * Test return from update a category
     *
     * @return void
     */
    public function testReturnUpdateCategory()
    {
        $this->mockCategoryModel->allows([
            "find" => $this->defaultCategory,
        ]);
        $this->mockResourceBuild->allows([
            "buildUpdate" => $this->defaultCategory,
        ]);

        $business = new CategoryBusiness($this->mockCategoryModel, $this->mockResourceBuild);
        $this->assertEquals($this->defaultCategory, $business->updateCategory(1, []));
    }

    /**
     * Test to delete a category
     *
     * @return void
     */
    public function testDeleteCategory()
    {
        $this->mockCategoryModel->allows([
            "destroy" => true,
            "find" => $this->defaultCategory
        ]);

        $business = new CategoryBusiness($this->mockCategoryModel, $this->mockResourceBuild);
        $this->assertTrue($business->deleteCategory(1));
    }

    /**
     * Test ModelNotFoundException from update a category
     *
     * @expectedException \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function testUpdateCategoryExceptionModelNotFoundException()
    {
        $this->mockCategoryModel->allows()
            ->find(1)
            ->andThrow(new \Illuminate\Database\Eloquent\ModelNotFoundException(), 'was not found');
        $this->mockResourceBuild->allows([
            "buildUpdate" => $this->defaultCategory,
        ]);

        $business = new CategoryBusiness($this->mockCategoryModel, $this->mockResourceBuild);
        $business->updateCategory(1, []);
    }

    /**
     * Test ModelNotFoundException from get a category
     *
     * @expectedException \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function testGetCategoryExceptionModelNotFoundException()
    {
        $this->mockCategoryModel->allows()
            ->find(1)
            ->andThrow(new \Illuminate\Database\Eloquent\ModelNotFoundException(), 'was not found');
        $this->mockResourceBuild->allows([
            "buildUpdate" => $this->defaultCategory,
        ]);

        $business = new CategoryBusiness($this->mockCategoryModel, $this->mockResourceBuild);
        $business->getCategory(1);
    }
}
