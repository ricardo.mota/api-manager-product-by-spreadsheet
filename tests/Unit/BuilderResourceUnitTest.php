<?php

namespace Tests\Unit;

use Tests\Setup;
use App\Models\Product;
use App\Builders\ResourceBuilder;

class BuilderResourceUnitTest extends Setup
{
    public $builderDefault;

    public function setUp()
    {
        parent::setUp();
        $this->builderDefault = new ResourceBuilder();
    }

    /**
     * Test to build create
     *
     * @return void
     */
    public function testBuildCreate()
    {
        $product = factory(Product::class)->make();
        $return = $this->builderDefault->buildCreate(new Product, $product->toArray());

        $this->assertEquals($product->toArray(), $return->toArray());
        $this->assertInstanceOf(Product::class, $return);
    }

    /**
     * Test to build update
     *
     * @return void
     */
    public function testBuildUpdate()
    {
        $product = factory(Product::class)->create();
        $data = ['name' => 'new name'];
        $return = $this->builderDefault->buildUpdate(new Product, $data, $product->lm);

        $this->assertContains($data['name'], $return->toArray());
        $this->assertInstanceOf(Product::class, $return);
    }
}
