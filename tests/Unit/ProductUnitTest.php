<?php

namespace Tests\Unit;

use Mockery;
use Tests\Setup;
use Illuminate\Support\Collection;
use App\Business\ProductBusiness;
use App\Builders\ResourceBuilder;
use App\Models\Product;

class ProductUnitTest extends Setup
{
    /**
     * @var mockProductModel
     */
    private $mockProductModel;

    /**
     * @var mockResourceBuild
     */
    private $mockResourceBuild;

    /**
     * @var defaultProduct
     */
    private $defaultProduct;


    public function setUp()
    {
        parent::setUp();
        $this->mockProductModel = Mockery::mock(Product::class);
        $this->mockResourceBuild = Mockery::mock(ResourceBuilder::class);
        $this->defaultProduct = factory(Product::class)->create();
    }

    /**
     * Test to get a product collection
     *
     * @return void
     */
    public function testGetAllProducts()
    {
        $this->mockProductModel->allows([
            "get" => collect([$this->defaultProduct]),
        ]);

        $business = new ProductBusiness($this->mockProductModel, $this->mockResourceBuild);
        $this->assertEquals(collect([$this->defaultProduct]), $business->getProducts());
        $this->assertInstanceOf(Collection::class, $business->getProducts());
    }

    /**
     * Test to get a product
     *
     * @return void
     */
    public function testGetProduct()
    {
        $this->mockProductModel->allows([
            "find" => $this->defaultProduct,
        ]);

        $business = new ProductBusiness($this->mockProductModel, $this->mockResourceBuild);
        $this->assertEquals($this->defaultProduct, $business->getProduct(1));
        $this->assertInstanceOf(Product::class, $business->getProduct(1));
    }

    /**
     * Test to create product
     *
     * @return void
     */
    public function testCreateProduct()
    {
        $this->mockResourceBuild->allows([
            "buildCreate" => $this->defaultProduct,
        ]);

        $business = new ProductBusiness($this->mockProductModel, $this->mockResourceBuild);
        $this->assertEquals($this->defaultProduct, $business->storeProduct([]));
        $this->assertInstanceOf(Product::class, $business->storeProduct([]));
    }

    /**
     * Test to update a product
     *
     * @return void
     */
    public function testUpdateProduct()
    {
        $this->mockProductModel->allows([
            "find" => $this->defaultProduct,
        ]);
        $this->mockResourceBuild->allows([
            "buildUpdate" => $this->defaultProduct,
        ]);

        $business = new ProductBusiness($this->mockProductModel, $this->mockResourceBuild);
        $this->assertInstanceOf(Product::class, $business->updateProduct(1, []));
    }

    /**
     * Test return from update a product
     *
     * @return void
     */
    public function testReturnUpdateProduct()
    {
        $this->mockProductModel->allows([
            "find" => $this->defaultProduct,
        ]);
        $this->mockResourceBuild->allows([
            "buildUpdate" => $this->defaultProduct,
        ]);

        $business = new ProductBusiness($this->mockProductModel, $this->mockResourceBuild);
        $this->assertEquals($this->defaultProduct, $business->updateProduct(1, []));
    }

    /**
     * Test to delete a product
     *
     * @return void
     */
    public function testDeleteProduct()
    {
        $this->mockProductModel->allows([
            "destroy" => true,
            "find" => $this->defaultProduct
        ]);

        $business = new ProductBusiness($this->mockProductModel, $this->mockResourceBuild);
        $this->assertTrue($business->deleteProduct(1));
    }

    /**
     * Test ModelNotFoundException from update a product
     *
     * @expectedException \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function testUpdateProductExceptionModelNotFoundException()
    {
        $this->mockProductModel->allows()
            ->find(1)
            ->andThrow(new \Illuminate\Database\Eloquent\ModelNotFoundException(), 'was not found');
        $this->mockResourceBuild->allows([
            "buildUpdate" => $this->defaultProduct,
        ]);

        $business = new ProductBusiness($this->mockProductModel, $this->mockResourceBuild);
        $business->updateProduct(1, []);
    }

    /**
     * Test ModelNotFoundException from get a product
     *
     * @expectedException \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function testGetProductExceptionModelNotFoundException()
    {
        $this->mockProductModel->allows()
            ->find(1)
            ->andThrow(new \Illuminate\Database\Eloquent\ModelNotFoundException(), 'was not found');
        $this->mockResourceBuild->allows([
            "buildUpdate" => $this->defaultProduct,
        ]);

        $business = new ProductBusiness($this->mockProductModel, $this->mockResourceBuild);
        $business->getProduct(1);
    }
}
