# API Manager Product by Spreadsheet

API Manager Product by Spreadsheet
### Installation

This API requires [PHP](http://www.php.net/) v7.0+ to run.

Clone the project:
```sh
$ git clone https://gitlab.com/ricardo.mota/api-manager-product-by-spreadsheet.git
```

Go to folder:
```sh
$ cd api-manager-product-by-spreadsheet
```

Install the dependencies:
```sh
$ composer install
```

Create your .env:
```sh
$ cp .env.example .env
```

Configure your .env:
```sh
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE={YOUR_NAME_DATABASE}
DB_USERNAME={YOUR_USERNAME}
DB_PASSWORD={YOUR_PASSWORD}

QUEUE_CONNECTION=database
```

Run the migrates and seed:
```sh
$ php artisan migrate && php artisan db:seed
```
Execute queue:
```sh
$ php artisan queue:work
```

## Request & Response Examples

You can download a postman json file: [postman](https://gitlab.com/ricardo.mota/api-manager-product-by-spreadsheet/blob/master/ApiSpreadsheet.json)

### API Resources


| Method | URI | Description |
| ------ | ------ | ------ |
| GET | [/api/products](#get-products) | List all products |
| GET | [/api/products/:id](#get-productsid) | Show a single product |
| PUT | [/api/products](#put-products) | Update a product |
| DELETE | [/api/products/:id](#delete-productsid) | Delete a product |
| POST | [/api/products/import](#post-productsimport) | Import a spreadsheet to store products |
| GET | [/api/products/import/status/:id](#get-productsimportstatusid) | Get spreadsheet import status |


### GET /products

Example: /api/products

Request body Response Success:
Status Code: 200

     [
        {
            "lm": 9001,
            "name": "Teste name 01",
            "free_shipping": 0,
            "description": "Teste Description 01",
            "price": 100
        },
        {
            "lm": 9002,
            "name": "Teste name 02",
            "free_shipping": 1,
            "description": "Teste Description 02",
            "price": 200
        }
    ]

### GET /products/:id

Example: /api/products/1

Request body Response Success:
Status Code: 200

    {
        "lm": 9001,
        "name": "Teste name 01",
        "free_shipping": 0,
        "description": "Teste Description 01",
        "price": 100
    }



### PUT /products

Example: /api/products

#### Input:

| Name | Type | Description |
| ------ | ------ | ------ |
| lm | integer | Inventory control number |
| name | string | Product name |
| free_shipping | boolean | Define if is free shipping or not |
| description | string | Product description |
| price | double | Product Price |

Request body Response Success:
Status Code: 200

    {
        "lm": 9001,
        "name": "Teste name 01",
        "free_shipping": 0,
        "description": "Teste Description 01",
        "price": 100
    }
    
### DELETE /products/:id

Example: /api/products/1

Request body Response Success:
Status Code: 200

     [
        {
            "lm": 9002,
            "name": "Teste name 02",
            "free_shipping": 1,
            "description": "Teste Description 02",
            "price": 200
        }
    ]
    
### post /products/import

Example: /products/import

#### Input:

| Name | Type | Description |
| ------ | ------ | ------ |
| spreadsheet | file | spreadsheet with product to import |

Request body Response Success:
Status Code: 200

    {
        "id": 1,
        "file": "kVHRduRsoYBsluBMNaXLwIogc2m7EU1YYowma7c8.xlsx"
    }

### GET /products/import/status/:id

Example: /products/import/status/1

Request body Response Success:
Status Code: 200

    {
        "id": 1,
        "job_id": 2,
        "success": 1
    }
