<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\ProductsImport;
use Log;
use Exception;

class ProcessImportSpreadsheet implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var String to keep spreadsheet name
     */
    public $spreadsheet;

    /**
     * @var String to keep disk name
     */
    public $disk;

    /**
     * Create a new job instance.
     *
     * @param string $spreadsheet
     * @param string $disk
     * @return void
     */
    public function __construct(string $spreadsheet, string $disk = 'local')
    {
        $this->spreadsheet = $spreadsheet;
        $this->disk = $disk;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Excel::import(new ProductsImport, $this->spreadsheet, $this->disk);
    }

    /**
     * The job failed to process.
     *
     * @param  Exception  $exception
     * @return void
     */
    public function failed(Exception $exception)
    {
        Log::warning($exception);
    }
}
