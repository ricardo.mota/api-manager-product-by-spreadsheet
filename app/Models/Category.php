<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model implements ModelInterface
{
    protected $table = 'categories';

    protected $fillable = [
        'id'
    ];
}
