<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model implements ModelInterface
{
    use SoftDeletes;

    protected $primaryKey = 'lm';
    protected $table = 'products';
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'lm',
        'name',
        'free_shipping',
        'description',
        'price',
        'category_id'
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];
}
