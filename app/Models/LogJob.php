<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LogJob extends Model implements ModelInterface
{
    protected $table = 'log_jobs';

    protected $fillable = [
        'job_id',
        'success'
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];
}
