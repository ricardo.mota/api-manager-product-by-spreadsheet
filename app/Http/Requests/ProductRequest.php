<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'lm'            => 'integer',
            'name'          => 'string|min:3|max:255',
            'free_shipping' => 'boolean',
            'description'   => 'string',
            'price'         => 'numeric'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'lm.integer' => '[:attribute] - must be an integer',
            'name.string' => '[:attribute] - must be a string',
            'name.min' => '[:attribute] - must be between-:min-:max',
            'name.max' => '[:attribute] - must be between-:min-:max',
            'free_shipping.boolean' => '[:attribute] - must be a boolean',
            'price.numeric' => '[:attribute] - must be a numeric'
        ];
    }
}
