<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductImportRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'spreadsheet' => 'required|mimes:xlsx,xlsm,xltx,xltm,xls,xlt,odsots,slk,xml,gnumeric,htm,html,csv,tsv'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'spreadsheet.required' => '[:attribute] - is required',
            'spreadsheet.mimes' => '[:attribute] - must be one of them: :values',
        ];
    }
}

