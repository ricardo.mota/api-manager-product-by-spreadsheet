<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductRequest;
use App\Business\ProductBusiness;

class ProductController extends Controller
{
    /**
     * variable to recive ProductBusiness
     */
    public $productBusiness;

    /**
     * Constructor controller
     *
     * @param ProductBusiness $productBusiness
     * @return void
     */
    public function __construct(ProductBusiness $productBusiness)
    {
        $this->productBusiness = $productBusiness;
    }

    /**
     * Returns a list of products
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return response()->json($this->productBusiness->getProducts());
    }

    /**
     * Returns a single product
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        return response()->json($this->productBusiness->getProduct($id));
    }

    /**
     * Update a product and return it
     *
     * @param $id
     * @param ProductRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function update(ProductRequest $request, $id)
    {
        return response()->json($this->productBusiness->updateProduct($id, $request->all()));
    }

    /**
     * Delete the product and returns the new list of products
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $this->productBusiness->deleteProduct($id);
        return response()->json($this->productBusiness->getProducts());
    }
}
