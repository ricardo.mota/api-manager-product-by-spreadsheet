<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductImportRequest;
use App\Business\ImportBusiness;
use Illuminate\Http\UploadedFile;
use Storage;

class ImportController extends Controller
{
    /**
     * variable to recive ImportBusiness
     */
    public $importBusiness;

    /**
     * Constructor controller
     *
     * @param ImportBusiness $importBusiness
     * @return void
     */
    public function __construct(ImportBusiness $importBusiness)
    {
        $this->importBusiness = $importBusiness;
    }

    /**
     * Import a spreadsheet
     *
     * @param ProductImportRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function import(ProductImportRequest $request)
    {
        $uploadName = $this->uploadFile($request->file('spreadsheet'));
        return response()->json($this->importBusiness->importSpreadsheet($uploadName, 'spreadsheets'));
    }

    /**
     * Get some spreadsheet status
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function status($id)
    {
        return response()->json($this->importBusiness->getStatusSpreadsheet($id));
    }

    /**
     * Upload file and return name
     *
     * @param UploadedFile $file
     * @return string
     */
    private function uploadFile(UploadedFile $file): string
    {
        return Storage::disk('spreadsheets')->put('', $file);
    }
}
