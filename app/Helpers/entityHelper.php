<?php

if (! function_exists('get_random_id_from_entity')) {

    /**
     * Get ramdom id from some entity
     *
     * @param ModelInterface $class
     * @param string $field
     * @return int
     */
    function get_random_id_from_entity(\App\Models\ModelInterface $class, string $field) {
        $ids = $class->select($field)->pluck($field)->toArray();
        $id = 0;

        do {
            $id = rand(1, 999999);
        } while(in_array($id, $ids));
        return $id;
    }

}
