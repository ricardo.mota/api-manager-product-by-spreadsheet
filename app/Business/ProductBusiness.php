<?php

namespace App\Business;

use App\Models\Product as ProductModel;
use App\Builders\ResourceBuilder;
use Illuminate\Support\Collection;
use App\Exceptions\LmAlreadyExistException;

class ProductBusiness extends AbstractBusiness
{
    /**
     * Constructor class
     * @access public
     * @param ProductModel $productModel
     * @param ResourceBuilder $resourceBuilder
     * @return void
     */
    public function __construct(ProductModel $productModel, ResourceBuilder $resourceBuilder)
    {
        $this->resourceModel   = $productModel;
        $this->resourceBuilder = $resourceBuilder;
    }

    /**
     * Returns a list of products
     * @access public
     * @return Collection
     */
    public function getProducts(): Collection
    {
        return parent::getResources();
    }

    /**
     * Returns a single product
     * @access public
     * @param int $productId
     * @return ProductModel
     */
    public function getProduct(int $productId): ProductModel
    {
        return parent::getResource($productId);
    }

    /**
     * Store a new product
     * @access public
     * @param array $data | array with new data
     * @return ProductModel
     */
    public function storeProduct(array $data): ProductModel
    {
        return parent::storeResource($data);
    }

    /**
     * Update a product
     * @access public
     * @param integer $productId | some product id
     * @param array $data | array with new data
     * @return ProductModel
     * @throws LmAlreadyExistException
     */
    public function updateProduct(int $productId, array $data): ProductModel
    {
        $this::validateModelExist($this->resourceModel, $productId);
        if (isset($data['lm'])) {
            $this->validateUsedLm($productId, $data['lm']);
        }
        $this->resourceModel = $this->resourceBuilder->buildUpdate($this->resourceModel, $data, $productId);
        $this->resourceModel->save();
        return $this->resourceModel;

        // return parent::updateResource($productId, $data);
    }

    /**
     * Delete a product
     * @access public
     * @param integer $productId
     * @return bool
     */
    public function deleteProduct(int $productId): bool
    {
        return parent::deleteResource($productId);
    }

    /**
     * @param int $resourceId
     * @param int $lm
     * @throws LmAlreadyExistException
     * @return void
     */
    private function validateUsedLm(int $resourceId, int $lm): void
    {
        $currentModel = $this->resourceModel->find($resourceId);
        $validateLm = $this->resourceModel->where('lm', $lm)->exists();

        if (($validateLm) && ($currentModel->lm != $lm)) {
            throw new LmAlreadyExistException(sprintf(
                'The lm \'%s\' already exist.',
                $lm
            ));
        }
    }
}
