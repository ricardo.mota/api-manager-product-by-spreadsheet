<?php

namespace App\Business;

use App\Models\LogJob as LogJobModel;
use App\Models\Job as JobModel;
use App\Jobs\ProcessImportSpreadsheet;
use Illuminate\Contracts\Bus\Dispatcher;
use App\Support\ThrowExceptionSupport;

class ImportBusiness extends AbstractBusiness
{
    /**
     * Throw Exception Support
     */
    use ThrowExceptionSupport;

    /**
     * Import a spreadsheet
     *
     * @access public
     * @param string $fileName
     * @param string $drive
     * @return array
     * @throws \Exception
     */
    public function importSpreadsheet(string $fileName, string $drive): array
    {
        $this::validateFileExist($fileName, $drive);
        $job = (new ProcessImportSpreadsheet($fileName, $drive))->onQueue('default');
        $return['id'] = app(Dispatcher::class)->dispatch($job);
        $return['file'] = $fileName;

        return $return;
    }

    /**
     * Get some spreadsheet status
     *
     * @access public
     * @param int $id
     * @return LogJobModel
     */
    public function getStatusSpreadsheet(int $id): LogJobModel
    {
        $logJobModel = new LogJobModel;
        $this::validateModelByField($logJobModel, 'job_id', $id);
        $log = $logJobModel->where('job_id', $id)->first();
        return $log;
    }
}
