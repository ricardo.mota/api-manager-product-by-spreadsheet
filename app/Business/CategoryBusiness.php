<?php

namespace App\Business;

use App\Models\Category as CategoryModel;
use App\Builders\ResourceBuilder;
use Illuminate\Support\Collection;

class CategoryBusiness extends AbstractBusiness
{
    /**
     * Constructor class
     * @access public
     * @param CategoryModel $categoryModel
     * @param ResourceBuilder $resourceBuilder
     * @return void
     */
    public function __construct(CategoryModel $categoryModel, ResourceBuilder $resourceBuilder)
    {
        $this->resourceModel   = $categoryModel;
        $this->resourceBuilder = $resourceBuilder;
    }

    /**
     * Returns a list of categories
     * @access public
     * @return Collection
     */
    public function getCategories(): Collection
    {
        return parent::getResources();
    }

    /**
     * Returns a single category
     * @access public
     * @param int $categoryId | some category id
     * @return CategoryModel
     */
    public function getCategory(int $categoryId): CategoryModel
    {
        return parent::getResource($categoryId);
    }

    /**
     * Store a new category
     * @access public
     * @param array $data | array with new data
     * @return CategoryModel
     */
    public function storeCategory(array $data): CategoryModel
    {
        return parent::storeResource($data);
    }

    /**
     * Update a category
     * @access public
     * @param integer $categoryId | some category id
     * @param array $data | array with new data
     * @return CategoryModel
     */
    public function updateCategory(int $categoryId, array $data): CategoryModel
    {
         return parent::updateResource($categoryId, $data);
    }

    /**
     * Delete a category
     * @access public
     * @param integer $categoryId | some category id
     * @return bool
     */
    public function deleteCategory(int $categoryId): bool
    {
        return parent::deleteResource($categoryId);
    }
}
