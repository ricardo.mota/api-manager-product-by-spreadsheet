<?php

namespace App\Imports;

use Log;
use Validator;
use Exception;
use App\Business\ProductBusiness;
use App\Business\CategoryBusiness;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class ProductsImport implements ToCollection
{
    /**
     * variable to receive ProductBusiness
     */
    public $productBusiness;

    /**
     * variable to receive CategoryBusiness
     */
    public $categoryBusiness;

    public function __construct()
    {
        $this->productBusiness = app()->make(ProductBusiness::class);
        $this->categoryBusiness = app()->make(CategoryBusiness::class);
    }

    /**
    * @param Collection $rows
    *
    * @return null
    */
    public function collection(Collection $rows)
    {
        $errors = [];
        $category = false;

        $validateCategoryRow = $this->validateCategoryRow($rows[0]);
        if ($validateCategoryRow) {
            $category = $this->processCategory($rows[0]);
        }

        foreach ($rows as $row) {
            if ($this->validateProductRow($row) && $category) {
                try {
                    $product = [
                        'lm'            => $row[0],
                        'name'          => $row[1],
                        'free_shipping' => $row[2],
                        'description'   => $row[3],
                        'price'         => $row[4],
                        'category_id'   => $category->getAttribute('id')
                    ];
                    $this->processProduct($product);
                } catch (Exception $e) {
                    Log::warning($e);
                    $errors[] = $row;
                }
            }
        }
    }

    /**
     * @param Collection $row
     * @return Category
     */
    private function processCategory(Collection $row): Category
    {
        $row = $this->makeCategoryRow($row);
        $validator = Validator::make($row, [
            'id' => 'unique:categories',
        ]);

        if ($validator->fails()) {
            $category = $this->categoryBusiness->getCategory($row['id']);
        } else {
            $category = $this->categoryBusiness->storeCategory(['id' => $row['id']]);
        }

        return $category;
    }

    /**
     * @param array $row
     * @return Product
     */
    private function processProduct(array $row): Product
    {
        $validator = Validator::make($row, [
            'lm' => 'unique:products',
        ]);

        if ($validator->fails()) {
            $product= $this->productBusiness->updateProduct($row['lm'], $row);
        } else {
            $product = $this->productBusiness->storeProduct($row);
        }

        return $product;
    }

    /**
     * @param Collection $row
     * @return bool
     */
    private function validateCategoryRow(Collection $row): bool
    {
        $row = $this->makeCategoryRow($row);

        $validator = Validator::make($row, [
            'name' => 'required|string|in:Category',
            'id' => 'required|integer',
        ]);

        if ($validator->fails()) {
            return false;
        }

        return true;
    }

    /**
     * @param Collection $row
     * @return bool
     */
    private function validateProductRow(Collection $row): bool
    {
        $row = $this->makeProductRow($row);

        $validator = Validator::make($row, [
            'lm' => 'required|integer',
            'name' => 'required|string|string',
            'free_shipping' => 'required',
            'description' => 'required|string',
            'price' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return false;
        }

        return true;
    }

    /**
     * @param Collection $row
     * @return array
     */
    private function makeCategoryRow(Collection $row): array
    {
        $newRow = [];
        $newRow['name'] = $row[0] ?? null;
        $newRow['id']   = $row[1] ?? null;

        return $newRow;
    }

    /**
     * @param Collection $row
     * @return array
     */
    private function makeProductRow(Collection $row): array
    {
        $newRow = [];
        $newRow['lm']            = $row[0] ?? null;
        $newRow['name']          = $row[1] ?? null;
        $newRow['free_shipping'] = $row[2] ?? null;
        $newRow['description']   = $row[3] ?? null;
        $newRow['price']         = $row[4] ?? null;

        return $newRow;
    }
}
