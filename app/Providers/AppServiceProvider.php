<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Queue;
use Illuminate\Queue\Events\JobProcessing;
use Illuminate\Queue\Events\JobProcessed;
use Illuminate\Queue\Events\JobFailed;

use App\Models\LogJob;
use Exception;
use Log;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        Queue::after(function (JobProcessed $event) {
            $this->afterJobProcessed($event);
        });

        Queue::failing(function (JobFailed $event) {
            $this->failingJobFailed($event);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
    }

    /**
     * Register log job after Processed
     *
     * @param JobProcessed $event
     * @return void
     */
    private function afterJobProcessed(JobProcessed $event)
    {
        try {
            $validate = LogJob::find($event->job->getJobId());
            if(!$validate){
                LogJob::create([
                    'job_id' => $event->job->getJobId(),
                    'success' => 1
                ]);
            }
        } catch (Exception $e) {
            Log::warning($e);
        }
    }

    /**
     * Register log job failing
     *
     * @param JobFailed $event
     * @return void
     */
    private function failingJobFailed(JobFailed $event)
    {
        try {
            $validate = LogJob::find($event->job->getJobId());
            if(!$validate){
                LogJob::create([
                    'job_id' => $event->job->getJobId(),
                    'success' => 0
                ]);
            }
        } catch (Exception $e) {
            Log::warning($e);
        }
    }
}
