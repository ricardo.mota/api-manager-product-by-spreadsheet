<?php

namespace App\Exceptions;

use Exception;

class ThereIsNoCategoryException extends Exception
{
}
