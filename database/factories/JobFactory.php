<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Job::class, function (Faker $faker) {
    return [
        'queue' => 'tests',
        'payload' => json_encode($faker->words()),
        'attempts' => $faker->boolean(),
        'reserved_at' => $faker->randomNumber(),
        'available_at' => $faker->randomNumber(),
        'created_at' => $faker->randomNumber()
    ];
});
