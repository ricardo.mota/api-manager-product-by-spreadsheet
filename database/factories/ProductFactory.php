<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Product::class, function (Faker $faker) {
    return [
        'lm' => get_random_id_from_entity(new App\Models\Product, 'lm'),
        'name' => $faker->text(20),
        'free_shipping' => $faker->boolean(),
        'description' => $faker->text(200),
        'price' => $faker->randomFloat(2, 1, 10000),
        'category_id' => factory(App\Models\Category::class)->create()
    ];
});
