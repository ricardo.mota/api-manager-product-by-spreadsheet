<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::resource('products', 'ProductController')->except([
    'store'
]);
Route::post('products/import', 'ImportController@import');
Route::get('products/import/status/{id}', 'ImportController@status');
